/*
  *
  * This file is a part of CoreTerminal. A terminal emulator for C Suite. Copyright 2019 CuboCore Group
  *
  *
  *
  * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General
  * Public License as published by the Free Software Foundation; either version 3 of the License, or (at your
  * option) any later version.
  *
  *
  *
  * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
  * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
  * License for more details.
  *
  *
  *
  * You should have received a copy of the GNU General Public License along with this program; if not, write to
  * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
  *
  */

#pragma once

#include <QFileSystemWatcher>

//
// The class is been used to provide a thread safe call for QFileSystemWatcher.
// Connecting/creating QFileSystemWatcher as a static member without having a
// class causes to initialize a non thread safe signals and slots.
//
// target was to remove this message from debug
// "QSocketNotifier: Can only be used with threads started with QThread"
//
// ref1:
// https://forum.qt.io/topic/29587/solved-qsocketnotifier-can-only-be-used-with-threads-started-with-qthread
// ref2:
// https://stackoverflow.com/questions/13888061/qsocketnotifier-can-only-be-used-with-threads-started-with-qthread-error/13977289#comment48180137_13977289
//

class globalObj
{
public:
    static QFileSystemWatcher *watcher();

    ~globalObj();
};
